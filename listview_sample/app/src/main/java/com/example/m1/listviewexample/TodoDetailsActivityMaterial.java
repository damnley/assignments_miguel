package com.example.m1.listviewexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.m1.listviewexample.view.WeatherRecyclerViewAdapter;

public class TodoDetailsActivityMaterial extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_details_material);
        WeatherRecyclerViewAdapter adapter = new WeatherRecyclerViewAdapter();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        WeatherRecyclerViewAdapter recyclerViewAdapter = new WeatherRecyclerViewAdapter();
        recyclerView.setAdapter(recyclerViewAdapter);
    }
}
