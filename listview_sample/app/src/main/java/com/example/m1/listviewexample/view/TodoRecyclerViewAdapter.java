package com.example.m1.listviewexample.view;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m1.listviewexample.R;
import com.example.m1.listviewexample.model.ToDo;

import java.util.List;

/**
 * Created by M18P on 9/21/2017.
 */

public class TodoRecyclerViewAdapter extends RecyclerView.Adapter<TodoRecyclerViewAdapter.ViewHolder> {
    private List<ToDo> mItems;
    private Context mContext;
    private AdapterView.OnItemClickListener;

    public TodoRecyclerViewAdapter(Context context, List<ToDo> items, AdapterView.OnItemClickListener){
        this.onItemClickListener;
        this.mItems = items;

    }


    /**
     * Method that given a list of To Do items will replace the current data on the adapter
     * and refresh the RecyclerView contents
     * the todo list of items to display on the recyclerview
     *
     *
     * */







    @Override
    public TodoRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_list_item, parent, false);
        TodoRecyclerViewAdapter.ViewHolder viewHolder = new TodoRecyclerViewAdapter.ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(TodoRecyclerViewAdapter.ViewHolder holder, int position) {
        ToDo toDo = mItems.get(position);
        holder.textTitle.setText(toDo.getTitle());
        if(toDo.isCompleted()){
            holder.imateStatus.setImageResource(R.mipmap.ic_done_black_24dp);
            holder.textTitle.setPaintFlags(
                    holder.textTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
            );
        } else {
            // No Image
            holder.imateStatus.setImageResource(android.R.color.transparent);
            holder.textTitle.setPaintFlags(
                holder.textTitle.getPaintFlags()
            );
        }


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public ToDo getItem(int index){ return mItems.get(index); }


    public void addItem(ToDo toDo){
        toDo.save(mContext);
        updateItems();
    }

    public void deleteItem(ToDo todo){
        todo.delete(mContext);
        update.items();
    }

    public void updateItems(){
        mItems = ToDo.getToDos(mContext);
        notifyDataSetChanged();
    }


    public void setDataSet(List<ToDo> items){
        mItems = items;
        this.notifyDataSetChanged();
    }





    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener listener{
        private TextView textTitle;
        private ImageView imateStatus;
        private TextView textContent;
        private AdapterView.OnItemClickListener onItemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            textTitle = (TextView) itemView.findViewById(R.id.lbl_todo_title);
            imateStatus = (ImageView) itemView.findViewById(R.id.image_todo_item);
            textContent = (TextView) itemView.findViewById(R.id.lbl_todo_contents);
            itemView.setOnClickListener(this);

        }




        @Override
        public void onClick(View view){
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());

        }
    }
}
