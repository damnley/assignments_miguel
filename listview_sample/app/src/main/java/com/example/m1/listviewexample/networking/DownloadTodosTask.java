package com.example.m1.listviewexample.networking;

import android.os.AsyncTask;
import com.example.m1.listviewexample.model.ToDo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class DownloadTodosTask extends AsyncTask <String, Void, List<ToDo>> {

    private TodoDownloadTaskInterface observer;
    public DownloadTodosTask(TodoDownloadTaskInterface inteface) {
        this.observer = inteface;
    }

    @Override
    protected List<ToDo> doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream)
            );

            StringBuilder stringBuilder = new StringBuilder();
            String inputString;

            while((inputString = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputString);
            }

            JSONArray jsonArray = new JSONArray(stringBuilder.toString());
            ArrayList<ToDo> todosList = new ArrayList<>(jsonArray.length() + 1);
            JSONObject jsonObject;
            for (int index = 0; index < jsonArray.length(); index++) {
                jsonObject = jsonArray.getJSONObject(index);
                ToDo todo = new ToDo(
                        jsonObject.getInt("id"),
                        jsonObject.getString("title"),
                        "");
                todosList.add(todo);
            }
            connection.disconnect();
            return todosList;

        } catch (Exception exception) {
            // Manage exception
        }
        // No data or an exception happened.
        return null;
    }

    @Override
    protected void onPostExecute(List<ToDo> toDoList) {
        observer.onTodoResult(toDoList);
    }
}
