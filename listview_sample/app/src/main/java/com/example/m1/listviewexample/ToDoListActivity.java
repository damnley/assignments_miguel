package com.example.m1.listviewexample;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import com.example.m1.listviewexample.model.ToDo;
import com.example.m1.listviewexample.model.TodoRequest;
import com.example.m1.listviewexample.view.TodoAdapter;
import com.example.m1.listviewexample.view.TodoRecyclerViewAdapter;

import java.util.List;

public class ToDoListActivity extends AppCompatActivity {

    private TodoRecyclerViewAdapter todoAdapter;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(
                    getResources().getColor(R.color.todo_action_bar)
            );
            actionBar.setBackgroundDrawable(colorDrawable);
        }

        // Retrieving the ListView object:
        recyclerView = (RecyclerView) findViewById(R.id.list_todos);

        // Set the ListView adapter
        setAdapter();
        // Set the required listeners
        setListeners();
    }

    private void setListeners() {



        // Set the To Do status as completed
        recyclerView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                ToDo toDo = (ToDo) todoAdapter.getItem(i);
                todoAdapter.deleteItem(toDo);
                return false;
            }
        });

        // Clear the completed To Dos
        Button todoDeleteButton = (Button) findViewById(R.id.btn_todo_delete);
        todoDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoAdapter.clearCompleted();
            }
        });

        FloatingActionButton todoAddButton = (FloatingActionButton) findViewById(R.id.btn_todo_add);
        todoAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToDoListActivity.this, ToDoDetailsActivity.class);
                startActivityForResult(intent, TodoRequest.NEW);
            }
        });
    }

    private void setAdapter(){

        // Show the To Do item details
        RecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Getting the item object from the adapter at the index position
                ToDo todoItem = (ToDo) todoAdapter.getItem(i);

                Intent intent = new Intent(ToDoListActivity.this, ToDoDetailsActivity.class);
                // Include the To Do item to display on the details activity as extra
                intent.putExtra(ToDoDetailsActivity.TODO_EXTRA, todoItem);

                // Start the To Do Details Activity
                startActivityForResult(intent, TodoRequest.EDIT);
            }
        });

        List <ToDo> items = fetchData();
        todoAdapter = new TodoRecyclerViewAdapter(getApplicationContext(), items, listener);

        // Set the adapter to the ListView
        recyclerView.setAdapter();
    }

    private List<ToDo> fetchData(){
        List todos = ToDo.getToDos(this);
        return todos;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            ToDo toDo = data.getParcelableExtra(ToDoDetailsActivity.TODO_EXTRA);
            switch(requestCode) {
                case TodoRequest.NEW:
                case TodoRequest.EDIT:
                    todoAdapter.addItem(toDo);
                    break;
                case TodoRequest.DELETE:
                    todoAdapter.deleteItem(toDo);
                    break;
                default:
                    break;
            }
        } else {
            Log.d(this.getClass().getSimpleName(), "ToDo activity result action cancelled or failed");
        }

    }
}
