package com.example.m1.listviewexample.view;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m1.listviewexample.R;

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder> {
    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.todo_list_item_material, parent, false);
        WeatherViewHolder weatherViewHolder = new WeatherViewHolder(view);
        return weatherViewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        // Set texts
        holder.header.setText("Today");
        holder.hour_one.setText("4:00 PM");
        //holder.image_forecast_one.setImageResource(R.id.sunny);
        holder.temperature_one.setText("8*");
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public static class WeatherViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView header;
        TextView hour_one;
        ImageView image_forecast_one;
        TextView temperature_one;

        public WeatherViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.weather_card_view_item);
            header = (TextView) itemView.findViewById(R.id.header);
            hour_one = (TextView) itemView.findViewById(R.id.hour_one);
            image_forecast_one = (ImageView) itemView.findViewById(R.id.image_forecast_one);
            temperature_one = (TextView) itemView.findViewById(R.id.temperature_one);
        }
    }
}


