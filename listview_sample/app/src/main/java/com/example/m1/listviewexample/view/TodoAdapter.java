package com.example.m1.listviewexample.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m1.listviewexample.R;
import com.example.m1.listviewexample.model.ToDo;

import java.util.Iterator;
import java.util.List;

public class TodoAdapter extends BaseAdapter {
    private List<ToDo> mItems;
    private Context mContext;

    public TodoAdapter(Context context, List<ToDo> items){
        this.mItems = items;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        // If there is no available view object to recycle, create one:
        if (view == null) {
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.todo_list_item, viewGroup, false);
        }

        // Get the object at the array index location:
        ToDo mItem = mItems.get(i);

        // Set its properties according to the item index:
        TextView todoTitleTextView  = (TextView) view.findViewById(R.id.lbl_todo_title);
            TextView todoContentTextView = (TextView) view.findViewById(R.id.lbl_todo_contents);
            ImageView imageView = (ImageView) view.findViewById(R.id.image_todo_item);

            todoTitleTextView.setText(mItem.getTitle());
            todoContentTextView.setText(mItem.getContent());
            if (mItem.isCompleted()) {
                imageView.setImageResource(R.mipmap.ic_done_black_24dp);
            }
            view.setTag(mItem.getId());
        return view;
    }

    /**
     * clearCompleted: Method to delete all To Do items that have the boolean value completed
     * as true from the collection
     */
    public void clearCompleted() {
        Iterator iterator = mItems.iterator();
        while(iterator.hasNext()){
            ToDo toDo = (ToDo) iterator.next();
            if (toDo.isCompleted()) {
                iterator.remove();
            }
        }
        notifyDataSetChanged();
    }


    /**
     * addItem: Method to add an item from the adapter's items collection given a To Do object.
     * @param toDo The toDo object to add to the list
     */
    public void addItem(ToDo toDo){
        toDo.save(mContext);
        updateItems();
    }

    /**
     * removeItem: Method to delete an item from the adapter's items collection given a To Do id.
     * @param toDo The to do instance to remove from the collection
     */
    public void deleteItem(ToDo toDo){
        toDo.delete(mContext);
        updateItems();
    }

    private void updateItems() {
        mItems = ToDo.getToDos(mContext);
        notifyDataSetChanged();
    }


}
