package com.example.m1.listviewexample.networking;

import com.example.m1.listviewexample.model.ToDo;

import java.util.List;

public interface TodoDownloadTaskInterface {
    void onTodoResult(List<ToDo> toDoList);
}
