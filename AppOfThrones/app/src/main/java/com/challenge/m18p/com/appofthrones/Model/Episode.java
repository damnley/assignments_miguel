package com.challenge.m18p.com.appofthrones.Model;

import java.io.Serializable;

/**
 * Created by M18P on 10/8/2017.
 */

public class Episode implements Serializable {
    private String title;
    private String year;
    private String released;
    private String season;
    private String epNumber;
    private String runTime;
    private String genre;
    private String director;
    private String writer;
    private String actor;
    private String plot;
    private String poster;
    private String tvRated;


    public Episode() {

        /**String title, String year, String released, String season, String epNumber,
         String runTime, String genre, String director, String writer,
         String actor, String plot, String poster, String tvRated/

     /*   this.title = title;
        this.year = year;
        this.released = released;
        this.season = season;
        this.epNumber = epNumber;
        this.runTime = runTime;
        this.genre = genre;
        this.director = director;
        this.writer = writer;
        this.actor = actor;
        this.plot = plot;
        this.poster = poster;
        this. tvRated = tvRated;
*/
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTvRated() {
        return tvRated;
    }

    public void setTvRated(String tvRated) {
        this.tvRated = tvRated;
    }

    public String getEpNumber() {
        return epNumber;
    }

    public void setEpNumber(String epNumber) {
        this.epNumber = epNumber;
    }
}
