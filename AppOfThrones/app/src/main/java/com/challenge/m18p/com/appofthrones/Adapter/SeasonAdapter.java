package com.challenge.m18p.com.appofthrones.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.challenge.m18p.com.appofthrones.EpisodesList;
import com.challenge.m18p.com.appofthrones.Model.Season;
import com.challenge.m18p.com.appofthrones.R;

import java.util.List;

public class SeasonAdapter extends RecyclerView.Adapter<SeasonAdapter.ViewHolder> {
    private Context context;
    private List<Season> seasonNumber;


    public SeasonAdapter(Context context, List seasonList){
        this.context = context;
        this.seasonNumber = seasonList;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.season_row, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(SeasonAdapter.ViewHolder holder, int position) {
        Season item = seasonNumber.get(position);
        holder.title.setText(item.getSeasonNumber());
    }

    @Override
    public int getItemCount() {
        return seasonNumber.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            title = (TextView) itemView.findViewById(R.id.seasonNumber);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ++position;
            Intent intent = new Intent(view.getContext(),EpisodesList.class);
            intent.putExtra("SEASON",position);
            context.startActivity(intent);


        }
    }
}
