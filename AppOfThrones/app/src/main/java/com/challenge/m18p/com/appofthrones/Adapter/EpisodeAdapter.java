package com.challenge.m18p.com.appofthrones.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.challenge.m18p.com.appofthrones.Model.Episode;
import com.challenge.m18p.com.appofthrones.R;

import java.util.List;

public class EpisodeAdapter extends RecyclerView.Adapter<EpisodeAdapter.ViewHolder> {
    private List<Episode> episodesList;


    public EpisodeAdapter(List episodeList){
        this.episodesList = episodeList;

    }

    @Override
    public EpisodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.episodes_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EpisodeAdapter.ViewHolder holder, int position) {
        holder.epTitle.setText("Fire and Blood");
        holder.epReleased.setText("May 2015");
        holder.epNumber.setText("Episode 8");
    }

    @Override
    public int getItemCount() {
        return episodesList.size();
    }

    public class ViewHolder  extends  RecyclerView.ViewHolder{
        public TextView epTitle;
        public TextView epReleased;
        public TextView epNumber;
        public ViewHolder(View itemView) {
            super(itemView);

            epTitle = (TextView) itemView.findViewById(R.id.epTitle);
            epReleased = (TextView) itemView.findViewById(R.id.epReleased);
            epNumber= (TextView) itemView.findViewById(R.id.epNumber);
        }
    }

    public void setEpisodes(List<Episode> episodeList) {
        this.episodesList = episodeList;
        this.notifyDataSetChanged();
    }
}
