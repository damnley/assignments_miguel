package com.challenge.m18p.com.appofthrones.Networking;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.challenge.m18p.com.appofthrones.Model.Episode;
import com.challenge.m18p.com.appofthrones.Util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NetworkingHandler {
    public static void fetchEpisodes(Context context, final NetworkingHandlerInterface observer, int season) {

        RequestQueue queue = Volley.newRequestQueue(context);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Constants.URL + season + Constants.API_KEY, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ArrayList<Episode> episodeList = new ArrayList<>();
                try{
                    JSONArray episodesArray = response.getJSONArray("episodesArray");

                    for (int i = 0; i < episodesArray.length(); i++) {

                        JSONObject episodeObj = episodesArray.getJSONObject(i);

                        Episode episode = new Episode();
                        episode.setTitle(episodeObj.getString("Title"));
                        episode.setYear("Released date " + episodeObj.getString("Released"));
                        episode.setEpNumber("Episode  " + episodeObj.getString("Episode"));
                        episodeList.add(episode);
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }

                observer.onEpisodeResults(episodeList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(jsonObjectRequest);

    }

    public interface NetworkingHandlerInterface {
        void onEpisodeResults(List<Episode> episodeList);
    }
}
