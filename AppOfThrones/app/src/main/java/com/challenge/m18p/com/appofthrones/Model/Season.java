package com.challenge.m18p.com.appofthrones.Model;

/**
 * Created by M18P on 10/8/2017.
 */

public class Season {


    public Season(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    private String seasonNumber;

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}
