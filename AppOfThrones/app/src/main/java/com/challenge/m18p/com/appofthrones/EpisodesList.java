package com.challenge.m18p.com.appofthrones;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.challenge.m18p.com.appofthrones.Adapter.EpisodeAdapter;
import com.challenge.m18p.com.appofthrones.Model.Episode;
import com.challenge.m18p.com.appofthrones.Networking.NetworkingHandler;

import java.util.ArrayList;
import java.util.List;

public class EpisodesList extends AppCompatActivity implements NetworkingHandler.NetworkingHandlerInterface {
    private int season;
    private RecyclerView episodeRecyclerView;
    private EpisodeAdapter episodeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes_list);
        season = Integer.parseInt(
                String.valueOf(getIntent().getExtras().getInt("SEASON")));
        Log.i("","Position: " + season);

        episodeRecyclerView = (RecyclerView) findViewById(R.id.episodeRecyclerViewID);
        episodeRecyclerView.setHasFixedSize(true);
        episodeRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        NetworkingHandler.fetchEpisodes(getApplicationContext(), this, season);

        episodeAdapter = new EpisodeAdapter(new ArrayList<Episode>());
        episodeRecyclerView.setAdapter(episodeAdapter);

    }


    @Override
    public void onEpisodeResults(List<Episode> episodeList) {
        episodeAdapter.setEpisodes(episodeList);
    }
}

