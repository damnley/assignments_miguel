package com.challenge.m18p.com.appofthrones;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.challenge.m18p.com.appofthrones.Adapter.SeasonAdapter;
import com.challenge.m18p.com.appofthrones.Model.Season;
import com.challenge.m18p.com.appofthrones.Model.Series;
import com.challenge.m18p.com.appofthrones.Util.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Series series;
    private TextView seriesTitle;
    private ImageView seriesImage;
    private TextView seriesYear;
    private TextView actors;
    private TextView genre;
    private TextView rating;
    private TextView writers;
    private TextView plot;
    private TextView seasons;
    private TextView runtime;
    private RecyclerView seasonRecyclerView;
    private RecyclerView.Adapter seasonAdapter;
    private List<Season> seasonList;
    public  int seasonn;


    private RequestQueue queue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);
        series = (Series) getIntent().getSerializableExtra("series");
        getSeriesDetails();

        setUpUI();

        seasonRecyclerView = (RecyclerView) findViewById(R.id.seasonRecyclerViewID);
        seasonRecyclerView.setHasFixedSize(true);
        seasonRecyclerView.setLayoutManager(new LinearLayoutManager((this)));

        seasonList = new ArrayList<>();
        Log.i("","For: "+seasonn);
        for(int i=0; i<8; ++i){
            Season item = new Season(
                    "Season " + (i+1));
                   seasonList.add(item);
        }
        seasonAdapter = new SeasonAdapter(this, seasonList);
        seasonRecyclerView.setAdapter(seasonAdapter);
    }



    private void getSeriesDetails(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Constants.URL_LEFT + Constants.API_KEY, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    if (response.has("Ratings")) {
                        JSONArray ratings = response.getJSONArray("Ratings");

                        String source = null;
                        String value = null;
                        if (ratings.length() > 0) {

                            JSONObject mRatings = ratings.getJSONObject(ratings.length() - 1);
                            source = mRatings.getString("Source");

                            value = mRatings.getString("Value");
                            rating.setText(source + " : " + value);

                        }else {
                            rating.setText("Ratings: N/A");
                        }

                        seriesTitle.setText(response.getString("Title"));

                        seriesYear.setText("Released: " + response.getString("Released"));
                        genre.setText("Genre: " + response.getString("Genre"));
                        writers.setText("Writers: " + response.getString("Writer"));
                        plot.setText("Plot: " + response.getString("Plot"));
                        runtime.setText("Runtime: " + response.getString("Runtime"));
                        actors.setText("Actors: " + response.getString("Actors"));

                        Picasso.with(getApplicationContext())
                                .load(response.getString("Poster"))
                                .into(seriesImage);

                        seasons.setText("Seasons: " + response.getString("totalSeasons"));

                        seasonn = Integer.parseInt(response.getString("totalSeasons"));
                        Log.d("","JSON: "+seasonn);

                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error:", error.getMessage());

            }
        });
        queue.add(jsonObjectRequest);

    }

    private void setUpUI() {

        seriesTitle = (TextView) findViewById(R.id.seriesTitleIDDets);
        seriesImage = (ImageView) findViewById(R.id.seriesImageIDDets);
        seriesYear = (TextView) findViewById(R.id.seriesReleaseIDDets);
        genre = (TextView) findViewById(R.id.genIDDet);
        rating = (TextView) findViewById(R.id.seriesRatingIDDet);
        writers = (TextView) findViewById(R.id.writersDet);
        plot = (TextView) findViewById(R.id.plotDet);
        seasons = (TextView) findViewById(R.id.seasonsDet);
        runtime = (TextView) findViewById(R.id.runtimeDet);
        actors = (TextView) findViewById(R.id.actorsDet);
    }

}
