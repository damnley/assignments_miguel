package com.example.m18p.weatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by M18P on 9/17/2017.
 */

public class Umbrella extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    Spinner spnGrades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.umbrella);

        spnGrades = (Spinner) findViewById(R.id.spnGrades);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.listItems));
        spnGrades.setAdapter(adapter);

        int selected= spnGrades.getSelectedItemPosition();
/*
        Integer selected = Integer.valueOf(spnGrades.getSelectedItem().toString());

    */
        Log.i("Grados:", ":"+selected+" selected");




    }


        public void btnSendCity(View view){
            EditText editText = (EditText) findViewById(R.id.txtSendCity);
            Button btnChange = (Button) findViewById(R.id.btnChange);
            String changeCity = editText.getText().toString();
            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra("SEARCH",changeCity);
            startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}