package com.example.m18p.weatherapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.graphics.Color;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity
{

    TextView txtTemp;
    TextView dateTextView;
    TextView txtWeather;
    TextView txtCity;
    ImageView imgWeather;

    public void callUmbrella(View view){
        Intent intent = new Intent(this,Umbrella.class);
        startActivity(intent);
    }


    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTemp = (TextView) findViewById(R.id.txtTemp);
        dateTextView = (TextView) findViewById(R.id.txtDate);
        txtWeather = (TextView) findViewById(R.id.txtWeather);
        txtCity = (TextView) findViewById(R.id.txtCity);

        imgWeather = (ImageView) findViewById(R.id.imgWeather);

        dateTextView.setText(getCurrentDate());


      //  String city=getIntent().getExtras()


        String searchW = "30030";

       searchW=getIntent().getExtras().getString("SEARCH");





        String url = "http://api.openweathermap.org/data/2.5/weather?q="+searchW+",US&appid=aa6267bbe68ca03b03b6bff7744db316&units=Imperial";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject responseObject) {
                        //txtTemp.setText("Response: " + response.toString());
                        Log.v("WEATHER", "Response: " + responseObject.toString());

                        try
                        {
                            JSONObject mainJSONObject = responseObject.getJSONObject("main");
                            JSONArray weatherArray = responseObject.getJSONArray("weather");
                            JSONObject firstWeatherObject = weatherArray.getJSONObject(0);

                            String temp = Integer.toString((int) Math.round(mainJSONObject.getDouble("temp")));
                            String weatherDescription = firstWeatherObject.getString("description");
                            String city = responseObject.getString("name");

                            ConstraintLayout  layerHeader = (ConstraintLayout) findViewById(R.id.layerHeader);
                            if(mainJSONObject.getDouble("temp")>=60) {
                                layerHeader.setBackgroundColor(Color.parseColor("#ffff8800"));
                            }else {
                                layerHeader.setBackgroundColor(Color.parseColor("#d4e2f7"));
                            }

                            txtTemp.setText(temp);
                            txtWeather.setText(weatherDescription);
                            txtCity.setText(city);


                            String imageURL = "http://openweathermap.org/img/w//";
                            imageURL += firstWeatherObject.getString("icon") +".png";

                            Picasso.with(getApplicationContext()).load(imageURL).into(imgWeather);



                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });

        // Access the RequestQueue through your singleton class.
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsObjRequest);
    }

    private String getCurrentDate ()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMM dd");
        String formattedDate = dateFormat.format(calendar.getTime());

        return formattedDate;
    }
}
