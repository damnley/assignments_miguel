package com.example.m18p.weatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ZipCodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.umbrella);
    }

    public void btnSendCity(View view){
        EditText editText = (EditText) findViewById(R.id.txtSendCity);
        Button btnChange = (Button) findViewById(R.id.btnChange);
        String changeCity = editText.getText().toString();
        Intent intent = new Intent(this,WeatherActivity.class);
        intent.putExtra("ZIPCODE",changeCity);
        startActivity(intent);
    }
}