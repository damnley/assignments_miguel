package com.example.m18p.weatherapp.utils;


import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util {
    public static String getCurrentDate ()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMM dd");
        String formattedDate = dateFormat.format(calendar.getTime());

        return formattedDate;
    }
}
